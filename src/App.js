import { useEffect, useState } from "react";
import {render} from "react-dom";
import Hand from './Hand';
import Deal from "./Deal";
import NewDeck from "./NewDeck";
import Shuffle from "./Shuffle";
import './style.css';

export default function App() {
    const [cards, setCards] = useState("");
    const [deck, setDeck] = useState();

    // get new deck on load
    useEffect(() => {
        requestNewDeck();
    },[]); // eslint-disable-line react-hooks/exhaustive-deps

    function shuffleDeck() {
        // fetch(`https://deckofcardsapi.com/api/deck/dc9khbqwrebi/shuffle/`)
        fetch(`https://deckofcardsapi.com/api/deck/${deck.deck_id}/shuffle/`)
            .then((res) => res.json())
            .then((data) => setDeck(data));
    }
    
    function requestNewDeck() {
        fetch(`https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1`)
            .then((res) => res.json())
            .then((data) => setDeck(data));
    }

    function getCards() {
        fetch(`https://deckofcardsapi.com/api/deck/${deck.deck_id}/draw/?count=5`)
            .then((res) => res.json())
            .then((data) => setCards(data.cards))
    }

    return (
        <div>
            <Deal deal={getCards}/>
            <Shuffle shuffle={shuffleDeck} />
            {/* <NewDeck getNewDeck={requestNewDeck}/> */}
            {cards && (<Hand cards={cards}/>)}
        </div>
    )
};

// render( <App / > , document.getElementById("root"));App