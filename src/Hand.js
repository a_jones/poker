// https: //github.com/goldfire/pokersolver
import {Hand} from 'pokersolver';

function setCardNumber(card) {
    switch (card.value) {
        case "ACE":
            card.number = 14;
            break;
        case "KING":
            card.number = 13;
            break;
        case "QUEEN":
            card.number = 12;
            break;
        case "JACK":
            card.number = 11;
            break;
        case "10":
            card.number =10;
            break;
        case "9":
            card.number =9;
            break;
        case "8":
            card.number =8;
            break;
        case "7":
            card.number =7;
            break;
        case "6":
            card.number =6;
            break;
        case "5":
            card.number =5;
            break;
        case "4":
            card.number =4;
            break;
        case "3":
            card.number =3;
            break;
        case "2":
            card.number =2;
            break;
        default:
            break;
    }
}

function getHandValue(cards) {
    let cardCodes = cards.map((card) => card.code);
    let hand = Hand.solve(cardCodes);
    // console.log(hand.name);
    // console.log(hand.descr);

    return hand;

}

function updateCardCode(card) {
    //pokersovler needs second value to be lowercase
    let cardCode = [...card.code];
    if(card.value == '10') {
        cardCode[0] = 'T';
    }

    cardCode[1] = cardCode[1].toLowerCase();
    card.code = cardCode.join('');
}

const Cards = ({cards}) => {
    cards.map((card) => setCardNumber(card));
    cards.map((card) => updateCardCode(card));
    cards.sort(function (a, b) {
        return a.number - b.number
    })

    
    const handValue = getHandValue(cards);

    const hand = cards.map((card) => 
        <li>
            <img src={card.image}></img>
        </li>
        )
        return (
            <div className='card-list'>
                <h2>{handValue.descr}</h2>
                <ul>{hand}</ul>
            </div>
        )
}

export default Cards;