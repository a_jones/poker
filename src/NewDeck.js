const NewDeck = ({getNewDeck}) => {
    return(
        <button onClick={getNewDeck}>New Deck</button>
    )
}

export default NewDeck;