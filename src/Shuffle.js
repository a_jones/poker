const Shuffle = ({shuffle}) => {
    return(
        <button onClick={shuffle}>Shuffle</button>
    )
}

export default Shuffle;